import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Gnome } from '../interfaces/gnome.interface';

@Injectable({
  providedIn: 'root'
})
export class GnomesService {
  constructor(private http: HttpClient) { }

  getGnomes() {
    return this.http.get('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json');
  }
}
