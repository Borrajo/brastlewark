import { Component, OnInit } from '@angular/core';
import { GnomesService } from './services/gnomes.service';
import { Gnome } from './interfaces/gnome.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public gnomes: Gnome[];
  public professions: string[] = [];
  public constructor(private gnomeService: GnomesService) { }
  public ngOnInit() {
    this.getGnomes();
  }

  public getGnomes() {
    this.gnomeService.getGnomes()
      .subscribe(
        (response) => {
          console.table(response['Brastlewark']);
          this.gnomes = response['Brastlewark'];
          this.professions = this.getProfessions();
          console.log(this.professions);
        },
        (error) => { }
      );
  }

  public getProfessions(): string[] {
    const array: string[] = [];
    this.gnomes.forEach((gnome) => {
      array.push(...gnome.professions
        .filter((profession) => !array.includes(profession))
      );
    });
    return array;
  }
}
